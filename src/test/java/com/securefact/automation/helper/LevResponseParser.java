package com.securefact.automation.helper;

public class LevResponseParser extends JsonParser{
    public LevResponseParser(String jsonStr) {
        super(jsonStr);
    }

    public String getEntityName(){
        return jsonPath.get("results.entityName[0]");
    }

    public String getResultId(){
        return jsonPath.get("results.resultId[0]").toString();
    }

    public String getOrderId(){
        Object orderId = jsonPath.get("orderId");

        return orderId == null ? null: orderId.toString();
    }

    public String getErrorField(){
        return jsonPath.get("errors.field[0]");
    }

    public String getError(){
        return jsonPath.get("errors.error[0]");
    }

    public String getErrorMsg(){
        return jsonPath.get("errors.message[0]");
    }

//    public String getpdfStatus(){
//        return jsonPath.get("status");
//    }

    public String getPdfMessage(){
        return jsonPath.get("message");
    }

    public String getPdfDocument(){
        return jsonPath.get("pdfdocument");
    }

    public String getDocumentType(){
        return jsonPath.get("documentType");
    }

    public String getStatus(){
        return jsonPath.get("status");
    }

    public String getDataEntityName(){
        return jsonPath.get("entity.name");
    }
}
