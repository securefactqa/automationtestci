package com.securefact.automation.helper;

import io.restassured.path.json.JsonPath;

public class SidniResponceParser extends JsonParser{

    public SidniResponceParser(String jsonStr){
        super(jsonStr);
    }


    public Boolean getVerifiedResult(){
        return jsonPath.get("verified");
    }

    public String getReason(){
        return jsonPath.get("reason");
    }

    public String getIndividualName(){
        return jsonPath.get("individualName");
    }

}
