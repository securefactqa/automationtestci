package com.securefact.automation.helper.dataprovider;

import com.securefact.automation.util.CSVUtil;
import org.testng.annotations.DataProvider;

import java.io.IOException;
import java.lang.reflect.Method;

public class RestfulTestDataProvider {

    @DataProvider(name = "getSidniTestData")
    public static Object[][] getSidniTestData(Method method) {
        return CSVUtil.getTestData(method.getDeclaringClass());
    }

    @DataProvider(name = "getSidniQA1TestData")
    public static Object[][] getSidniQA1TestData(Method method) {
        return CSVUtil.getTestData(method.getDeclaringClass().getSimpleName() + "_qa1");
    }

    @DataProvider(name = "getSidniQA1FlinksTestData")
    public static Object[][] getSidniQA1FlinksTestData(Method method) {
        return CSVUtil.getTestData(method.getDeclaringClass().getSimpleName() + "_qa1_flinks");
    }

    @DataProvider(name = "getSidniPreprodTestData")
    public static Object[][] getSidniPreprodTestData(Method method) {
        return CSVUtil.getTestData(method.getDeclaringClass().getSimpleName() + "_preprod");
    }

    @DataProvider(name = "getSidniProdTestData")
    public static Object[][] getSidniProdTestData(Method method) {
        return CSVUtil.getTestData(method.getDeclaringClass().getSimpleName() + "_prod");
    }

    @DataProvider(name = "getSidniQA2TestData")
    public static Object[][] getSidniQA2TestData(Method method) {
        return CSVUtil.getTestData(method.getDeclaringClass().getSimpleName() + "_qa2");
    }

    @DataProvider(name = "getLevPositiveTestData")
    public static Object[][] getLevPositiveTestData(Method method){
        return CSVUtil.getLEVTestData(method.getDeclaringClass().getSimpleName() + "_p");
    }

    @DataProvider(name = "getLevNegativeTestData")
    public static Object[][] getLevNegativeTestData(Method method){
        return CSVUtil.getLEVTestData(method.getDeclaringClass().getSimpleName() + "_n");
    }

    @DataProvider(name = "getLevQA1SmokingPositiveTestData")
    public static Object[][] getLevQA1SmokingPositiveTestData(Method method){
        return CSVUtil.getLEVTestData(method.getDeclaringClass().getSimpleName() + "_qa1_p");
    }

    @DataProvider(name = "getLevQA1SmokingNegativeTestData")
    public static Object[][] getLevQA1SmokingNegativeTestData(Method method){
        return CSVUtil.getLEVTestData(method.getDeclaringClass().getSimpleName() + "_qa1_n");
    }

    @DataProvider(name = "getLevQA2SmokingPositiveTestData")
    public static Object[][] getLevQA2SmokingPositiveTestData(Method method){
        return CSVUtil.getLEVTestData(method.getDeclaringClass().getSimpleName() + "_qa2_p");
    }

    @DataProvider(name = "getLevQA2SmokingNegativeTestData")
    public static Object[][] getLevQA2SmokingNegativeTestData(Method method){
        return CSVUtil.getLEVTestData(method.getDeclaringClass().getSimpleName() + "_qa2_n");
    }

    @DataProvider(name = "getLevPreprodSmokingTestData")
    public static Object[][] getLevPreprodSmokingTestData(Method method){
        return CSVUtil.getLEVTestData(method.getDeclaringClass().getSimpleName() + "_preprod");
    }

    @DataProvider(name = "getLevProdSmokingTestData")
    public static Object[][] getLevProdSmokingTestData(Method method){
        return CSVUtil.getLEVTestData(method.getDeclaringClass().getSimpleName() + "_prod");
    }

//    public static Object[][] getLevProfileRequestTestData(Method method){
//        StringBuffer fileName = new StringBuffer(method.getDeclaringClass().getSimpleName()).append("_profile");
//        return CSVUtil.getTestData(fileName.toString());
//    }
}
