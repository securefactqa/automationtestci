package com.securefact.automation.helper;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeDriverService;
import org.openqa.selenium.chrome.ChromeOptions;

import java.io.File;

/**
 * @Author: William Guo
 */
public class ChromeDriverManager extends DriverManager {
    private ChromeDriverService chromeService;

    @Override
    protected void startService() {
        if (null == chromeService) {
            try {
                chromeService = new ChromeDriverService.Builder()
                        .usingDriverExecutable(new File(System.getProperty("user.dir") + "\\executables\\chromedriver.exe"))
                        .usingAnyFreePort()
                        .build();
                chromeService.start();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    protected void stopService() {
        if (null != chromeService && chromeService.isRunning())
            chromeService.stop();
    }

    @Override
    protected void createDriver() {
        ChromeOptions options = new ChromeOptions();
        options.addArguments("test-type");
        driver = new ChromeDriver(chromeService, options);
    }
}
