package com.securefact.automation.helper;

import com.securefact.automation.helper.ChromeDriverManager;
import com.securefact.automation.helper.DriverManager;

public class DriverManagerFactory {
    public static DriverManager getManager(String browser) {

        DriverManager driverManager;

        switch(browser.toLowerCase()) {
            case "chrome":
                driverManager = new ChromeDriverManager();
                break;

            default:
                driverManager = new ChromeDriverManager();
                break;
        }

        return  driverManager;
    }
}
