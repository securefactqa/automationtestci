package com.securefact.automation.page;

import com.securefact.automation.helper.DriverManager;
import com.securefact.automation.helper.DriverManagerFactory;
import com.securefact.automation.util.property.AutomationProperties;
import com.securefact.automation.util.property.SirisPropertyEnum;
import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

/**
 * This is the super class that all page tests need to inherent from
 * In this class, the driver is initiated based on the properties file; and login to homepage is done. These two
 * operations can be reused by all other page test
 *
 * @Author: William Guo
 */
public abstract class PageTest {
    protected WebDriver driver;
    protected DriverManager driverManager;
    protected final Logger logger = LoggerFactory.getLogger(this.getClass().getSimpleName());

    @BeforeClass
    public void setupDriver() {
        driverManager = DriverManagerFactory.getManager(AutomationProperties.getProperty(SirisPropertyEnum.BROWSER_TO_USE));
        driver = driverManager.getDriver();
        driver.manage().window().maximize();
    }

    @AfterClass
    public void tearDown() {
        driverManager.quitDriver();
    }
}
