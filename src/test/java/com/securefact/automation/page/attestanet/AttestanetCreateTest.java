package com.securefact.automation.page.attestanet;

import com.securefact.automation.enumeration.JurisdictionEnum;
import com.securefact.automation.page.PageTest;
import com.securefact.automation.util.property.PropertyReader;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class AttestanetCreateTest extends PageTest {
    private WorkspacePage workspacePage;


    @BeforeMethod
    public void gotoPage(){
        driver.get(PropertyReader.INSTANCE.getValue("BASE_URL"));
        LoginPage attestanetLoginPage = new LoginPage(this.driver);
        workspacePage = attestanetLoginPage.login("william.concentra", "Securefact!@#");
    }

    @Test
    public void testCreateAttestanet(){
        AttestanetPage attestanetPage = workspacePage.createAttestanet();
        LevSearchResultPage levSearchResultPage = attestanetPage
                .inputReferenceName("fromWilliam01")
                .inputEntityName("abc co")
                .selectJurisdiction(JurisdictionEnum.ONTARIO)
                .clickSubmit();
        SelectAttestorPage selectAttestorPage = levSearchResultPage.clickFirstEligibleResult();
        selectAttestorPage.inputFirstName("QQ")
                .inputLastName("AA")
                .inputEmail("william.guo@securefact.com")
                .inputConfirmEmail("william.guo@securefact.com")
                .orderAttestation();
    }
}
