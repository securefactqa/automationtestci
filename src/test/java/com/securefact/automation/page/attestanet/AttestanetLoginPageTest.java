package com.securefact.automation.page.attestanet;

import com.securefact.automation.page.PageTest;
import com.securefact.automation.util.property.PropertyReader;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 * @Author: William Guo
 */
public class AttestanetLoginPageTest extends PageTest {
    private LoginPage attestanetLoginPage;

    @BeforeMethod
    public void gotoPage(){
        driver.get(PropertyReader.INSTANCE.getValue("BASE_URL"));
        attestanetLoginPage = new LoginPage(this.driver);
    }

    @Test
    public void testLogin() {
        WorkspacePage workspacePage = attestanetLoginPage.login("williamsf", "Securefact!@#");
        Assert.assertEquals("My !Workspace", driver.getTitle());
    }

}
