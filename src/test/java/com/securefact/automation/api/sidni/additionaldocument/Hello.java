package com.securefact.automation.api.sidni.additionaldocument;

import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@CucumberOptions(
        dryRun=true,
        strict=true,
        monochrome = true,
        features = {"src/test/resources/features/hello.feature"}
)
public class Hello extends AbstractTestNGCucumberTests {
    private final Logger logger = LoggerFactory.getLogger(this.getClass().getSimpleName());


}
