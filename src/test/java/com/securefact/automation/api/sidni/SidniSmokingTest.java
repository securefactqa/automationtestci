package com.securefact.automation.api.sidni;

import com.securefact.automation.helper.SidniResponceParser;
import com.securefact.automation.helper.dataprovider.RestfulTestDataProvider;
import com.securefact.automation.util.property.AutomationProperties;
import com.securefact.automation.util.property.SIDniPropertyEnum;
import io.restassured.response.Response;
import org.assertj.core.api.SoftAssertions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.Test;

public class SidniSmokingTest extends SidniTest{
    private final Logger logger = LoggerFactory.getLogger(this.getClass().getSimpleName());

    @Test(
            dataProvider = "getSidniQA1TestData",
            dataProviderClass = RestfulTestDataProvider.class
    )
    public void qa1SmokingTest(String requestStr, String expectedResponseStr) {
        positiveTest(requestStr, expectedResponseStr,
                AutomationProperties.getProperty(SIDniPropertyEnum.SIDNI_QA1_WS_AUTHKEY).trim(),
                AutomationProperties.getProperty(SIDniPropertyEnum.SIDNI_QA1_WS_URI).trim());
    }

    @Test(
            dataProvider = "getSidniQA1FlinksTestData",
            dataProviderClass = RestfulTestDataProvider.class
    )
    public void qa1FlinksSmokingTest(String requestStr, String expectedResponseStr) {
        positiveTest(requestStr, expectedResponseStr,
                AutomationProperties.getProperty(SIDniPropertyEnum.SIDNI_QA1_WS_AUTHKEY).trim(),
                AutomationProperties.getProperty(SIDniPropertyEnum.SIDNI_QA1_FLINKS_WS_URI).trim());
    }

    @Test(
            dataProvider = "getSidniQA2TestData",
            dataProviderClass = RestfulTestDataProvider.class
    )
    public void qa2SmokingTest(String requestStr, String expectedResponseStr) {
        positiveTest(requestStr, expectedResponseStr,
                AutomationProperties.getProperty(SIDniPropertyEnum.SIDNI_QA2_WS_AUTHKEY).trim(),
                AutomationProperties.getProperty(SIDniPropertyEnum.SIDNI_QA2_WS_URI).trim());
    }

    @Test(
            dataProvider = "getSidniPreprodTestData",
            dataProviderClass = RestfulTestDataProvider.class
    )
    public void preprodSmokingTest(String requestStr, String expectedResponseStr) {
        positiveTest(requestStr, expectedResponseStr,
                AutomationProperties.getProperty(SIDniPropertyEnum.SIDNI_PREPROD_WS_AUTHKEY).trim(),
                AutomationProperties.getProperty(SIDniPropertyEnum.SIDNI_PREPROD_WS_URI).trim());
    }

    @Test(
            dataProvider = "getSidniProdTestData",
            dataProviderClass = RestfulTestDataProvider.class
    )
    public void prodSmokingTest(String requestStr, String expectedResponseStr) {
        positiveTest(requestStr, expectedResponseStr,
                AutomationProperties.getProperty(SIDniPropertyEnum.SIDNI_PROD_WS_AUTHKEY).trim(),
                AutomationProperties.getProperty(SIDniPropertyEnum.SIDNI_PROD_WS_URI).trim());
    }
}
