package com.securefact.automation.api.sidni.additionaldocument;

import cucumber.api.CucumberOptions;
import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumber.api.testng.AbstractTestNGCucumberTests;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.Test;

/**
 *
 */
@CucumberOptions(
        dryRun=true,
        strict=true,
        monochrome = true,
        features = {"src/test/resources/features/SIR9394.feature"},
        plugin = {"rerun:build/cucumber/SIR9394.txt", "json:build/cucumber/SIR9394.json"}
)
public class TestSIR9393 extends AbstractTestNGCucumberTests {
    private final Logger logger = LoggerFactory.getLogger(this.getClass().getSimpleName());

}
