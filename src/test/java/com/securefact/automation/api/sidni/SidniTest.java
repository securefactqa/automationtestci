package com.securefact.automation.api.sidni;

import com.securefact.automation.helper.SidniResponceParser;
import com.securefact.automation.helper.dataprovider.RestfulTestDataProvider;
import org.assertj.core.api.SoftAssertions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.Test;
import io.restassured.response.Response;

public class SidniTest {
    private final Logger logger = LoggerFactory.getLogger(this.getClass().getSimpleName());

    public void positiveTest(String requestStr, String expectedResponseStr, String key, String sidniUri) {

        Response response = SIDniWS.sendSIDniRequest(requestStr, key, sidniUri);

        SidniResponceParser actualResponceParser = new SidniResponceParser(response.asString());
        SidniResponceParser expectedResponceParser = new SidniResponceParser(expectedResponseStr);

        SoftAssertions.assertSoftly(s -> {
            s.assertThat(actualResponceParser.getVerifiedResult()).isEqualTo(expectedResponceParser.getVerifiedResult());
            s.assertThat(actualResponceParser.getReason()).isEqualTo(expectedResponceParser.getReason());
        });
    }
}
