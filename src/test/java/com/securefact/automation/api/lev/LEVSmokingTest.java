package com.securefact.automation.api.lev;

import com.securefact.automation.helper.dataprovider.RestfulTestDataProvider;
import com.securefact.automation.util.property.AutomationProperties;
import com.securefact.automation.util.property.LEVPropertyEnum;
import org.testng.annotations.Test;

public class LEVSmokingTest extends LevTest{

    @Test(
            dataProvider = "getLevQA1SmokingPositiveTestData",
            dataProviderClass = RestfulTestDataProvider.class
    )
    public void qa1SmokingPositiveTest(String[] testData){

        super.positiveTest(testData,
                AutomationProperties.getProperty(LEVPropertyEnum.LEV_QA1_AUTHKEY).trim(),
                AutomationProperties.getProperty(LEVPropertyEnum.LEV_QA1_SEARCH_URI).trim(),
                AutomationProperties.getProperty(LEVPropertyEnum.LEV_QA1_PROFILE_REQUEST_URI).trim(),
                AutomationProperties.getProperty(LEVPropertyEnum.LEV_QA1_GET_DATA_URI).trim(),
                AutomationProperties.getProperty(LEVPropertyEnum.LEV_QA1_GET_PDF_URI).trim(),
                10000);
    }

    @Test(
            dataProvider = "getLevQA1SmokingNegativeTestData",
            dataProviderClass = RestfulTestDataProvider.class
    )
    public void qa1NegativeTest(String[] testData){
        super.negativeTest(testData);
    }

    @Test(
            dataProvider = "getLevQA2SmokingPositiveTestData",
            dataProviderClass = RestfulTestDataProvider.class
    )
    public void qa2SmokingPositiveTest(String[] testData){
        super.positiveTest(testData,
                AutomationProperties.getProperty(LEVPropertyEnum.LEV_QA2_AUTHKEY).trim(),
                AutomationProperties.getProperty(LEVPropertyEnum.LEV_QA2_SEARCH_URI).trim(),
                AutomationProperties.getProperty(LEVPropertyEnum.LEV_QA2_PROFILE_REQUEST_URI).trim(),
                AutomationProperties.getProperty(LEVPropertyEnum.LEV_QA2_GET_DATA_URI).trim(),
                AutomationProperties.getProperty(LEVPropertyEnum.LEV_QA2_GET_PDF_URI).trim(),
                30000);
    }

    @Test(
            dataProvider = "getLevQA2SmokingNegativeTestData",
            dataProviderClass = RestfulTestDataProvider.class
    )
    public void qa2SmokingNegativeTest(String[] testData){
        super.pdfNegativeTest(testData,
                AutomationProperties.getProperty(LEVPropertyEnum.LEV_QA2_AUTHKEY).trim(),
                AutomationProperties.getProperty(LEVPropertyEnum.LEV_QA2_SEARCH_URI).trim(),
                AutomationProperties.getProperty(LEVPropertyEnum.LEV_QA2_PROFILE_REQUEST_URI).trim(),
                AutomationProperties.getProperty(LEVPropertyEnum.LEV_QA2_GET_DATA_URI).trim(),
                AutomationProperties.getProperty(LEVPropertyEnum.LEV_QA2_GET_PDF_URI).trim());
    }

    @Test(
            dataProvider = "getLevPreprodSmokingTestData",
            dataProviderClass = RestfulTestDataProvider.class
    )
    public void preprodSmokingTest(String[] testData){
        super.positiveTest(testData,
                AutomationProperties.getProperty(LEVPropertyEnum.LEV_PREPROD_AUTHKEY).trim(),
                AutomationProperties.getProperty(LEVPropertyEnum.LEV_PREPROD_SEARCH_URI).trim(),
                AutomationProperties.getProperty(LEVPropertyEnum.LEV_PREPROD_PROFILE_REQUEST_URI).trim(),
                AutomationProperties.getProperty(LEVPropertyEnum.LEV_PREPROD_GET_DATA_URI).trim(),
                AutomationProperties.getProperty(LEVPropertyEnum.LEV_PREPROD_GET_PDF_URI).trim(),
                10000);
    }

    @Test(
            dataProvider = "getLevProdSmokingTestData",
            dataProviderClass = RestfulTestDataProvider.class
    )
    public void prodSmokingTest(String[] testData){
        super.positiveTest(testData,
                AutomationProperties.getProperty(LEVPropertyEnum.LEV_PROD_AUTHKEY).trim(),
                AutomationProperties.getProperty(LEVPropertyEnum.LEV_PROD_SEARCH_URI).trim(),
                AutomationProperties.getProperty(LEVPropertyEnum.LEV_PROD_PROFILE_REQUEST_URI).trim(),
                AutomationProperties.getProperty(LEVPropertyEnum.LEV_PROD_GET_DATA_URI).trim(),
                AutomationProperties.getProperty(LEVPropertyEnum.LEV_PROD_GET_PDF_URI).trim(),
                10000);
    }
}
