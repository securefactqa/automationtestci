package com.securefact.automation.api.lev;

import com.securefact.automation.helper.LevResponseParser;
import com.securefact.automation.helper.dataprovider.RestfulTestDataProvider;
import com.securefact.automation.util.JsonUtil;
import io.restassured.response.Response;
import org.assertj.core.api.SoftAssertions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

/**
 *
 * @Author: William Guo
 *
 */
public class SIR9216Test extends LevTest{

    //set all jurisdiction for all documents as automated except for PE and NU
    @Test(
            dataProvider = "getLevPositiveTestData",
            dataProviderClass = RestfulTestDataProvider.class
    )
    public void positiveTest(String[] testData){
        //lev search request
        Response levSearchResponse = LevWS.sendLEVSearchRequest(testData[0]);

        String resultId = new LevResponseParser(levSearchResponse.asString()).getResultId();
        logger.debug("resultId: " + resultId);

        //profile/additionalDocument request
        String documentRequest = JsonUtil.replaceWith(testData[1], resultId);
        logger.info(documentRequest);
        Response documentResponse = LevWS.sendProfileRequest(documentRequest);

        String orderId = new LevResponseParser(documentResponse.asString()).getOrderId();
        assertThat(orderId).isNotNull();

        String orderIdRequest = new StringBuffer("{\"orderId\": ").append(orderId).append("}").toString();

        //request pdf
        Response pdfResponse = LevWS.sendGetPdfRequest(orderIdRequest);
        LevResponseParser pdfResponseParser = new LevResponseParser(pdfResponse.asString());
        LevResponseParser expectedPdfResponseParser = new LevResponseParser(testData[2]);

        String documentType = new LevResponseParser(testData[1]).getDocumentType();
        if(documentType == null){
            SoftAssertions.assertSoftly(s -> {
                s.assertThat(pdfResponseParser.getStatus()).isEqualTo(expectedPdfResponseParser.getStatus());
                s.assertThat(pdfResponseParser.getPdfMessage()).isEqualTo(pdfResponseParser.getPdfMessage());
            });
        } else {
            SoftAssertions.assertSoftly(s -> {
                s.assertThat(pdfResponseParser.getStatus()).isIn(expectedPdfResponseParser.getStatus(), "Completed");
                s.assertThat(pdfResponseParser.getPdfMessage()).isIn(pdfResponseParser.getPdfMessage(), "PDFPLACEHOLDER");
            });
        }

    }

    //set all jurisdiction for all documents as manual
    @Test(
            dataProvider = "getLevNegativeTestData",
            dataProviderClass = RestfulTestDataProvider.class
    )
    public void negativeTest(String[] testData){
        //lev search request
        Response levSearchResponse = LevWS.sendLEVSearchRequest(testData[0]);

        String resultId = new LevResponseParser(levSearchResponse.asString()).getResultId();
        logger.debug("resultId: " + resultId);

        //profile/additionalDocument request
        String documentRequest = JsonUtil.replaceWith(testData[1], resultId);
        logger.info(documentRequest);
        Response documentResponse = LevWS.sendProfileRequest(documentRequest);

        String orderId = new LevResponseParser(documentResponse.asString()).getOrderId();
        assertThat(orderId).isNotNull();

        String orderIdRequest = new StringBuffer("{\"orderId\": ").append(orderId).append("}").toString();

        //request pdf
        Response pdfResponse = LevWS.sendGetPdfRequest(orderIdRequest);
        LevResponseParser pdfResponseParser = new LevResponseParser(pdfResponse.asString());
        LevResponseParser expectedPdfResponseParser = new LevResponseParser(testData[2]);

        SoftAssertions.assertSoftly(s -> {
            s.assertThat(pdfResponseParser.getStatus()).isEqualTo(expectedPdfResponseParser.getStatus());
            s.assertThat(pdfResponseParser.getPdfMessage()).isEqualTo(pdfResponseParser.getPdfMessage());
        });

    }
}
