package com.securefact.automation.api.lev;

import com.securefact.automation.helper.LevResponseParser;
import com.securefact.automation.helper.dataprovider.RestfulTestDataProvider;
import com.securefact.automation.util.JsonUtil;
import com.securefact.automation.util.property.AutomationProperties;
import com.securefact.automation.util.property.LEVPropertyEnum;
import io.restassured.response.Response;
import org.assertj.core.api.SoftAssertions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

/**
 *
 * @Author: William Guo
 */
public class SIR9176Test extends LevTest{
//    private final Logger logger = LoggerFactory.getLogger(this.getClass().getSimpleName());

    /*
    @Test(
            dataProvider = "getLevPositiveTestData",
            dataProviderClass = RestfulTestDataProvider.class
    )
    public void positiveTest(String[] testData) {
        Response levSearchResponse = LevWS.sendLEVSearchRequest(testData[0]);

        LevResponseParser levResponseParser = new LevResponseParser(levSearchResponse.asString());
        String resultId = levResponseParser.getResultId();
        logger.debug("resultId: " + resultId);

        String documentRequest = JsonUtil.replaceWith(testData[1], resultId);
        logger.info(documentRequest);
        Response documentResponse = LevWS.sendProfileRequest(documentRequest);

        levResponseParser = new LevResponseParser(documentResponse.asString());
        String orderId = levResponseParser.getOrderId();
        assertThat(orderId).isNotNull();
    }

    @Test(
            dataProvider = "getLevNegativeTestData",
            dataProviderClass = RestfulTestDataProvider.class
    )
    public void negativeTest(String[] testData) {
        Response levSearchResponse = LevWS.sendLEVSearchRequest(testData[0]);
        logger.debug(levSearchResponse.asString());
        LevResponseParser levResponseParser = new LevResponseParser(levSearchResponse.asString());
        //logger.info("EntityName: " + levResponseParser.getEntityName());
        String resultId = levResponseParser.getResultId();
        logger.debug("resultId: " + resultId);

        String documentRequest = JsonUtil.replaceWith(testData[1], resultId);
        logger.info(documentRequest);
        Response documentResponse = LevWS.sendProfileRequest(documentRequest);

        LevResponseParser docResponseParser = new LevResponseParser(documentResponse.asString());
        LevResponseParser expectedDocResponseParser = new LevResponseParser(testData[2]);

        SoftAssertions.assertSoftly(s -> {
            s.assertThat(docResponseParser.getErrorField()).isEqualTo(expectedDocResponseParser.getErrorField());
            s.assertThat(docResponseParser.getError()).isEqualTo(expectedDocResponseParser.getError());
            s.assertThat(docResponseParser.getErrorMsg()).isEqualTo(expectedDocResponseParser.getErrorMsg());
        });
    }
*/
    @Test(
            dataProvider = "getLevPositiveTestData",
            dataProviderClass = RestfulTestDataProvider.class
    )
    public void positiveTest(String[] testData){
        super.positiveTest(testData,
                AutomationProperties.getProperty(LEVPropertyEnum.LEV_QA1_AUTHKEY).trim(),
                AutomationProperties.getProperty(LEVPropertyEnum.LEV_QA1_SEARCH_URI).trim(),
                AutomationProperties.getProperty(LEVPropertyEnum.LEV_QA1_PROFILE_REQUEST_URI).trim(),
                AutomationProperties.getProperty(LEVPropertyEnum.LEV_QA1_GET_DATA_URI).trim(),
                AutomationProperties.getProperty(LEVPropertyEnum.LEV_QA1_GET_PDF_URI).trim(),
                10000);
    }

    @Test(
            dataProvider = "getLevNegativeTestData",
            dataProviderClass = RestfulTestDataProvider.class
    )
    public void negativeTest(String[] testData){
        super.negativeTest(testData);
    }

}
