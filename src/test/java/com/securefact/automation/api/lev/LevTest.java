package com.securefact.automation.api.lev;

import com.securefact.automation.helper.LevResponseParser;
import com.securefact.automation.helper.dataprovider.RestfulTestDataProvider;
import com.securefact.automation.util.JsonUtil;
import io.restassured.response.Response;
import org.assertj.core.api.SoftAssertions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

import static org.assertj.core.api.Assertions.assertThat;

public class LevTest {
    protected Logger logger = LoggerFactory.getLogger(this.getClass().getSimpleName());

    public void positiveTest(String[] testData, String authenticationKey, String searchUri,String documentUri, String dataUri, String pdfUri, long waitSeconds ) {
        //lev search request
        Response levSearchResponse = LevWS.sendLEVSearchRequest(testData[0], authenticationKey, searchUri);

        String resultId = new LevResponseParser(levSearchResponse.asString()).getResultId();
        logger.debug("resultId: " + resultId);

        //profile/additionalDocument request
        String documentRequest = JsonUtil.replaceWith(testData[1], resultId);
        logger.info(documentRequest);
        Response documentResponse = LevWS.sendProfileRequest(documentRequest, authenticationKey, documentUri);

        String orderId = new LevResponseParser(documentResponse.asString()).getOrderId();

        String orderIdRequest = new StringBuffer("{\"orderId\": ").append(orderId).append("}").toString();

        waitawhile(waitSeconds);

        //request data
        Response dataResponse = LevWS.sendGetDataRequest(orderIdRequest, authenticationKey, dataUri);
        LevResponseParser dataResponseParser = new LevResponseParser(dataResponse.asString());
        LevResponseParser expectedDataResponse = new LevResponseParser(testData[3]);

        if(expectedDataResponse.getOrderId() == null){
            logger.info("Actual error: " + dataResponseParser.getError());
            SoftAssertions.assertSoftly(s -> {
                s.assertThat(dataResponseParser.getError()).isEqualTo(expectedDataResponse.getError());
                s.assertThat(dataResponseParser.getErrorMsg()).isEqualTo(expectedDataResponse.getErrorMsg());
            });
        } else
            SoftAssertions.assertSoftly(s -> {
                s.assertThat(dataResponseParser.getOrderId()).isEqualTo(orderId);
                s.assertThat(dataResponseParser.getDataEntityName()).isEqualTo(expectedDataResponse.getDataEntityName());
                s.assertThat(dataResponseParser.getStatus()).isEqualTo(expectedDataResponse.getStatus());
            });

        //request pdf
        Response pdfResponse = LevWS.sendGetPdfRequest(orderIdRequest, authenticationKey, pdfUri);
        LevResponseParser pdfResponseParser = new LevResponseParser(pdfResponse.asString());
        LevResponseParser expectedPdfResponse = new LevResponseParser(testData[2]);

        SoftAssertions.assertSoftly(s -> {
            s.assertThat(pdfResponseParser.getOrderId()).isEqualTo(orderId);
            s.assertThat(pdfResponseParser.getPdfDocument().substring(0, 9)).isEqualTo(expectedPdfResponse.getPdfDocument().substring(0, 9));
            s.assertThat(pdfResponseParser.getStatus()).isEqualTo(expectedPdfResponse.getStatus());
        });
    }

    public void negativeTest(String[] testData) {
        Response levSearchResponse = LevWS.sendLEVSearchRequest(testData[0]);
        logger.debug(levSearchResponse.asString());
        //logger.info("EntityName: " + levResponseParser.getEntityName());
        String resultId = new LevResponseParser(levSearchResponse.asString()).getResultId();
        logger.debug("resultId: " + resultId);

        String documentRequest = JsonUtil.replaceWith(testData[1], resultId);
        logger.info(documentRequest);
        Response documentResponse = LevWS.sendProfileRequest(documentRequest);

        LevResponseParser docResponseParser = new LevResponseParser(documentResponse.asString());
        LevResponseParser expectedDocResponseParser = new LevResponseParser(testData[2]);

        SoftAssertions.assertSoftly(s -> {
            s.assertThat(docResponseParser.getErrorField()).isEqualTo(expectedDocResponseParser.getErrorField());
            s.assertThat(docResponseParser.getError()).isEqualTo(expectedDocResponseParser.getError());
            s.assertThat(docResponseParser.getErrorMsg()).isEqualTo(expectedDocResponseParser.getErrorMsg());
        });
    }

    public void pdfNegativeTest(String[] testData, String authenticationKey, String searchUri,String documentUri, String dataUri, String pdfUri) {
        Response levSearchResponse = LevWS.sendLEVSearchRequest(testData[0], authenticationKey, searchUri);
        logger.debug(levSearchResponse.asString());
        //logger.info("EntityName: " + levResponseParser.getEntityName());
        String resultId = new LevResponseParser(levSearchResponse.asString()).getResultId();
        logger.debug("resultId: " + resultId);

        String documentRequest = JsonUtil.replaceWith(testData[1], resultId);
        logger.info(documentRequest);
        Response documentResponse = LevWS.sendProfileRequest(documentRequest, authenticationKey, documentUri);

        String orderId = new LevResponseParser(documentResponse.asString()).getOrderId();
        assertThat(orderId).isNotNull();

        String orderIdRequest = new StringBuffer("{\"orderId\": ").append(orderId).append("}").toString();
        //request data
        Response dataResponse = LevWS.sendGetDataRequest(orderIdRequest, authenticationKey, dataUri);
        LevResponseParser dataResponseParser = new LevResponseParser(dataResponse.asString());
        SoftAssertions.assertSoftly(s -> {
            s.assertThat(dataResponseParser.getError()).isEqualTo("REQUEST_INVALID");
            s.assertThat(dataResponseParser.getErrorMsg()).isEqualTo("Data is not available for this type of document");
        });

        //request pdf
        Response pdfResponse = LevWS.sendGetPdfRequest(orderIdRequest, authenticationKey, pdfUri);
        LevResponseParser pdfResponseParser = new LevResponseParser(pdfResponse.asString());
        LevResponseParser expectedPdfResponseParser = new LevResponseParser(testData[2]);

        SoftAssertions.assertSoftly(s -> {
            s.assertThat(pdfResponseParser.getStatus()).isEqualTo(expectedPdfResponseParser.getStatus());
            s.assertThat(pdfResponseParser.getPdfMessage()).isEqualTo(pdfResponseParser.getPdfMessage());
        });
    }

    private void waitawhile(long seconds){
        try
        {
            Thread.sleep(seconds);
        }
        catch(InterruptedException ex)
        {
            Thread.currentThread().interrupt();
        }
    }

}
