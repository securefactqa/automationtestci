Feature: to verify request for additional document

Scenario Outlines: to verify the error message while sending invalid request for additional document

	Given LEV is ready

	When send LEV search request and get resultId
		And send following request for additional document
		"""
		{
        	"resultId" : 162530,
             "customerReference":"<customerReference>",
             "documentType":"<documentType>"
        }
		"""
	Then should get following response
		"""
		{"errors": [{
           "field": "<field>",
           "error": "<error>",
           "message": "<message>"
        }]}
		"""

	Examples:
	 |   customerReference      |   documentType   |  field     |  error        |                         message                       |
	 |   TESTING123             |  Certificate     |  resultId  | FIELD_INVALID |               resultId is invalid                     |
