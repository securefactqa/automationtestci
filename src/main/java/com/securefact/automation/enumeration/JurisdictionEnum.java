package com.securefact.automation.enumeration;

public enum JurisdictionEnum {
    ONTARIO("ON", "Ontario"),
    QUEBEC("QC","Quebec"),
    ALBERTA("AB", "Alberta"),
    FEDERAL("CA", "Federal"),
    BRITISH_COLUMBIA("BC", "British Columbia"),
    ALL("ALL", "All Jurisdictions");

    private String value;
    private String desc;

    private JurisdictionEnum(String value, String desc){
        this.value = value;
        this.desc = desc;
    }

    public String getValue(){
        return this.value;
    }

    public String getDesc(){
        return this.desc;
    }
}
