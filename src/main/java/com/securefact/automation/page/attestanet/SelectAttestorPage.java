package com.securefact.automation.page.attestanet;

import com.securefact.automation.page.BasePage;
import com.securefact.automation.util.webelement.ElementUtil;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class SelectAttestorPage extends BasePage {
    @FindBy(id="val_first_name")
    private WebElement firstName;
    @FindBy(id="val_last_name")
    private WebElement lastName;
    @FindBy(id="val_email")
    private WebElement email;
    @FindBy(id="val_confirm_Email")
    private WebElement confirmEmail;
    @FindBy(xpath="//button[contains(text(),'ORDER ATTESTATION')]")
    private WebElement orderButton;

    public SelectAttestorPage(WebDriver driver) {
        super(driver);
    }

    public SelectAttestorPage inputFirstName(String firstNameStr){
        ElementUtil.inputTextField(firstName, firstNameStr);

        return this;
    }

    public SelectAttestorPage inputLastName(String lastNameStr){
        ElementUtil.inputTextField(lastName, lastNameStr);

        return this;
    }

    public SelectAttestorPage inputEmail(String emailStr){
        ElementUtil.inputTextField(email, emailStr);

        return this;
    }

    public SelectAttestorPage inputConfirmEmail(String confirmEmailStr){
        ElementUtil.inputTextField(confirmEmail, confirmEmailStr);

        return this;
    }

    public void orderAttestation(){
        ElementUtil.waitForElementToBeClickable(driver, orderButton);
        orderButton.click();
    }
}
