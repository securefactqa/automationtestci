package com.securefact.automation.page.attestanet;

import com.securefact.automation.page.BasePage;
import com.securefact.automation.util.webelement.ElementUtil;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;

import java.time.Duration;
import java.util.function.Function;

import static java.util.concurrent.TimeUnit.SECONDS;

public class WorkspacePage extends BasePage {

    @FindBy(css="#securfact-menu > li > a")
    private WebElement createNewOrderBtn;
    @FindBy(css="#securfact-menu > li > ul > li > a")
    private WebElement attestanetBtn;

    public WorkspacePage (WebDriver driver){
        super(driver);
    }

    public AttestanetPage createAttestanet(){
        Actions action = new Actions(this.driver);
        action.moveToElement(createNewOrderBtn).perform();

        ElementUtil.waitForElementVisible(driver, attestanetBtn);
//        Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)
//                .withTimeout(Duration.ofSeconds(30))
//                .pollingEvery(Duration.ofMillis(500))
//                .ignoring(NoSuchElementException.class);
//
//        WebElement attestanetBtn = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("#securfact-menu > li > ul > li > a")));

        action.moveToElement(attestanetBtn).click().perform();

        return new AttestanetPage(driver);
    }
}
