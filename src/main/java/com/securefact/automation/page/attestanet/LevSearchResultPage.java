package com.securefact.automation.page.attestanet;

import com.securefact.automation.page.BasePage;
import com.securefact.automation.util.webelement.ElementUtil;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class LevSearchResultPage extends BasePage {

    @FindBy(css="input[class='form-control']")
    private WebElement searchInput;
    @FindBy(id="dynamic-table-order")
    private WebElement resultTable;
    @FindBy(css="#dynamic-table-order > tbody > tr")
    private List<WebElement> allSearchResults;

    public LevSearchResultPage(WebDriver driver) {
        super(driver);
        ElementUtil.waitForElementVisible(driver, searchInput);
    }

    public SelectAttestorPage clickFirstEligibleResult(){
        for(WebElement row: allSearchResults){
            ElementUtil.waitForElementVisible(driver, row);
            if (null != row.findElement(By.cssSelector("td.green"))) {
                row.click();

                return new SelectAttestorPage(driver);
            }
        }
        return null;
    }
}
