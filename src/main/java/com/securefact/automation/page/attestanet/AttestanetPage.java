package com.securefact.automation.page.attestanet;

import com.securefact.automation.enumeration.JurisdictionEnum;
import com.securefact.automation.page.BasePage;
import com.securefact.automation.util.webelement.ElementUtil;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class AttestanetPage extends BasePage {
    @FindBy(id="ReferenceNumber")
    private WebElement referenceNumber;
    @FindBy(id="EntityName")
    private WebElement entityName;
    @FindBy(id="Jurisdiction")
    private WebElement jurisdiction;
    @FindBy(xpath="//button[contains(text(),'Search')]")
//    @FindBy(css="button[class='btn btn-success']")
    private WebElement searchBtn;

    public AttestanetPage(WebDriver driver) {
        super(driver);

        ElementUtil.waitForElementVisible(driver, referenceNumber);
    }

    public AttestanetPage inputReferenceName(String reference) {
        referenceNumber.clear();
        referenceNumber.sendKeys(reference);

        return this;
    }

    public AttestanetPage inputEntityName(String entityNameStr){
        entityName.clear();
        entityName.sendKeys(entityNameStr);

        return this;
    }

    public AttestanetPage selectJurisdiction(JurisdictionEnum jurisdictionEnum){
        Select select = new Select(jurisdiction);
        select.selectByVisibleText(jurisdictionEnum.getDesc());

        return this;
    }

    public LevSearchResultPage clickSubmit(){
        searchBtn.click();
        ElementUtil.waitForElementDisappear(driver, referenceNumber, 20);
        return new LevSearchResultPage(driver);
    }
}
