package com.securefact.automation.util;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.json.JSONObject;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * @Author: William Guo
 */
public class JsonUtil {
    private static final Logger logger = LoggerFactory.getLogger(JsonUtil.class);

    public static String getJsonFile(Class<?> className) throws IOException {

        return new String(Files.readAllBytes(Paths.get(
                new StringBuffer("src/test/resources/testdata/")
                        .append(className.getSimpleName())
                        .append(".json")
                        .toString())), "UTF-8");
    }

    public static String convertObjToJson(Object obj) {
        try {
            String convertedObject = new ObjectMapper()
                    .configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false)
                    .setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.ANY)
                    .setSerializationInclusion(JsonInclude.Include.NON_NULL)
                    .writerWithDefaultPrettyPrinter().writeValueAsString(obj);

            return convertedObject;
        } catch (Exception ex) {
            logger.error("Could not convert the class " + obj.getClass().getSimpleName());
            logger.error(ex.getMessage());
            return null;
        }

    }

    public static String replaceWith(String originalJson, String newFieldValue){
        JSONObject jsonObj = new JSONObject(originalJson);
        jsonObj.remove("resultId");
        jsonObj.put("resultId", newFieldValue);

        return jsonObj.toString();
    }
}
