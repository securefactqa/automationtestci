package com.securefact.automation.util.property;

/**
 * @author william.guo
 */
public interface IPropertyEnum {
    public String getKeyText();

    public String getDescription();
}
