package com.securefact.automation.util.property;

/**
 *  @author william.guo
 */
public enum SIDniPropertyEnum implements IPropertyEnum{
    SIDNI_QA1_WS_URI("SIDNI_QA1_WS_URI", ""),
    SIDNI_QA1_FLINKS_WS_URI("SIDNI_QA1_FLINKS_WS_URI", ""),
    SIDNI_QA1_WS_AUTHKEY("SIDNI_QA1_WS_AUTHKEY", ""),
    SIDNI_QA2_WS_URI("SIDNI_QA2_WS_URI", ""),
    SIDNI_QA2_WS_AUTHKEY("SIDNI_QA2_WS_AUTHKEY", ""),
    SIDNI_PREPROD_WS_URI("SIDNI_PREPROD_WS_URI", ""),
    SIDNI_PREPROD_WS_AUTHKEY("SIDNI_PREPROD_WS_AUTHKEY", ""),
    SIDNI_PROD_WS_URI("SIDNI_PROD_WS_URI", ""),
    SIDNI_PROD_WS_AUTHKEY("SIDNI_PROD_WS_AUTHKEY", ""),
    USER_REFERENCE("USER_REFERENCE","")
    ;

    private String key;
    private String description;

    private SIDniPropertyEnum(String key, String description){
        this.key = key;
        this.description = description;
    }

    public String getKeyText(){
        return this.key;
    }

    public String getDescription(){
        return this.description;
    }
}
