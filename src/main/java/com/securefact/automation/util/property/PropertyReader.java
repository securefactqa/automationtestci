package com.securefact.automation.util.property;

import java.io.FileInputStream;
import java.util.Properties;

/**
 * @Author: William Guo
 */
public enum PropertyReader {
    INSTANCE;

    private Properties property = new Properties();

    private PropertyReader(){

        try {
            FileInputStream in = new FileInputStream("src/main/resources/common/automation.properties");
            property.load(in);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getValue(String key) {
        return property.getProperty(key).trim();
    }
}
