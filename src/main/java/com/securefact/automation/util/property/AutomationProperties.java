package com.securefact.automation.util.property;

import java.util.Map;

/**
 * @Author: William Guo
 */
public class AutomationProperties {
    private static Map<String, String> env = System.getenv();

    public static String getProperty(IPropertyEnum key) {
        if (env.get(key.getKeyText()) != null) {
            return env.get(key.getKeyText());
        } else {
            return PropertyReader.INSTANCE.getValue(key.getKeyText());
        }
    }
}
