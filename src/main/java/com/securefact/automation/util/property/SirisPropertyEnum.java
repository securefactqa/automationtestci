package com.securefact.automation.util.property;

public enum SirisPropertyEnum implements IPropertyEnum{
    BROWSER_TO_USE("BROWSER_TO_USE", "");

    private String key;
    private String description;

    private SirisPropertyEnum(String key, String description){
        this.key = key;
        this.description = description;
    }

    public String getKeyText() {
        return this.key;
    }

    public String getDescription() {
        return this.description;
    }
}
