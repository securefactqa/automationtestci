package com.securefact.automation.util.property;

public enum LEVPropertyEnum implements IPropertyEnum{
    LEV_QA1_SEARCH_URI("LEV_QA1_SEARCH_URI", ""),
    LEV_QA1_PROFILE_REQUEST_URI("LEV_QA1_PROFILE_REQUEST_URI", ""),
    LEV_QA1_GET_DATA_URI("LEV_QA1_GET_DATA_URI", ""),
    LEV_QA1_GET_PDF_URI("LEV_QA1_GET_PDF_URI", ""),
    LEV_QA1_AUTHKEY("LEV_QA1_AUTHKEY", ""),

    LEV_QA2_SEARCH_URI("LEV_QA2_SEARCH_URI", ""),
    LEV_QA2_PROFILE_REQUEST_URI("LEV_QA2_PROFILE_REQUEST_URI", ""),
    LEV_QA2_GET_DATA_URI("LEV_QA2_GET_DATA_URI", ""),
    LEV_QA2_GET_PDF_URI("LEV_QA2_GET_PDF_URI", ""),
    LEV_QA2_AUTHKEY("LEV_QA2_AUTHKEY", ""),

    LEV_PREPROD_SEARCH_URI("LEV_PREPROD_SEARCH_URI", ""),
    LEV_PREPROD_PROFILE_REQUEST_URI("LEV_PREPROD_PROFILE_REQUEST_URI", ""),
    LEV_PREPROD_GET_DATA_URI("LEV_PREPROD_GET_DATA_URI", ""),
    LEV_PREPROD_GET_PDF_URI("LEV_PREPROD_GET_PDF_URI", ""),
    LEV_PREPROD_AUTHKEY("LEV_PREPROD_AUTHKEY", ""),

    LEV_PROD_SEARCH_URI("LEV_PROD_SEARCH_URI", ""),
    LEV_PROD_PROFILE_REQUEST_URI("LEV_PROD_PROFILE_REQUEST_URI", ""),
    LEV_PROD_GET_DATA_URI("LEV_PROD_GET_DATA_URI", ""),
    LEV_PROD_GET_PDF_URI("LEV_PROD_GET_PDF_URI", ""),
    LEV_PROD_AUTHKEY("LEV_PROD_AUTHKEY", "");

    private String key;
    private String description;

    private LEVPropertyEnum(String key, String description){
        this.key = key;
        this.description = description;
    }

    @Override
    public String getKeyText() {
        return this.key;
    }

    @Override
    public String getDescription() {
        return this.description;
    }
}
