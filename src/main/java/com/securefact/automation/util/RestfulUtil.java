package com.securefact.automation.util;

import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static io.restassured.RestAssured.given;

public class RestfulUtil {
    private static final Logger logger = LoggerFactory.getLogger(RestfulUtil.class);

    public static Response sendRequest(String authenticationKey, String seaviceURL, String request){
        if (logger.isDebugEnabled()) {
            logger.debug("Request/Response is: ");
            return given().
                    auth().
                    basic(authenticationKey, "").
                    contentType(ContentType.JSON).body(request).log().everything(true).
                    expect().
                    when().
                    post(seaviceURL).
                    then().
                    log().all(true).
                    extract().
                    response();

        } else {
            return given().
                    auth().
                    basic(authenticationKey, "").
                    contentType(ContentType.JSON).body(request).
                    expect().
                    when().
                    post(seaviceURL).
                    then().
                    extract().
                    response();
        }
    }
}
