package com.securefact.automation.util;

import com.opencsv.CSVParser;
import com.opencsv.CSVParserBuilder;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.stream.StreamSupport;


/**
 * @Author: William Guo
 */
public class CSVUtil {
    private static final Logger logger = LoggerFactory.getLogger(CSVUtil.class);

    public static Object[][]  getTestData(String fileName){
        String csvFile =  new StringBuffer("src/test/resources/testdata/")
                .append(fileName)
                .append(".csv")
                .toString();

        CSVParser parser = new CSVParserBuilder().withSeparator(',').build();
        try (BufferedReader br = Files.newBufferedReader(Paths.get(csvFile),
                StandardCharsets.UTF_8);
             CSVReader reader = new CSVReaderBuilder(br).withCSVParser(parser)
                     .build()){

            Object[][] arrays = StreamSupport.stream(reader.spliterator(), false)
                    .skip(1)
                    .map(line -> {
                        String[] newLine = new String[2];
                        newLine[0] = line[1];
                        newLine[1] = line[2];
                        return newLine;
                    })
                    .map(line -> Arrays.stream(line).toArray())
                    .toArray(Object[][]::new);

            return arrays;
        }catch(IOException e){
            logger.error(e.getMessage());
            return null;
        }
    }

    public static Object[][]  getTestData(Class<?> className) {
        return getTestData(className.getSimpleName());
    }

    public static Object[][]  getLEVTestData(String fileName){
        String csvFile =  new StringBuffer("src/test/resources/testdata/")
                .append(fileName)
                .append(".csv")
                .toString();

        CSVParser parser = new CSVParserBuilder().withSeparator(',').build();
        try (BufferedReader br = Files.newBufferedReader(Paths.get(csvFile),
                StandardCharsets.UTF_8);
             CSVReader reader = new CSVReaderBuilder(br).withCSVParser(parser)
                     .build()){

            Object[][] arrays = StreamSupport.stream(reader.spliterator(), false)
                    .skip(1)
                    .map(line -> {
                        String[] newLine = new String[4];
                        newLine[0] = line[1];
                        newLine[1] = line[2];
                        newLine[2] = line[3];
                        newLine[3] = line[4];
                        return newLine;
                    })
                    .map(line -> new Object[]{line})
                    .toArray(Object[][]::new);

            return arrays;
        }catch(IOException e){
            logger.error(e.getMessage());
            return null;
        }
    }
}
