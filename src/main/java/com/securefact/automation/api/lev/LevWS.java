package com.securefact.automation.api.lev;

import com.securefact.automation.util.RestfulUtil;
import com.securefact.automation.util.property.AutomationProperties;
import com.securefact.automation.util.property.LEVPropertyEnum;
import io.restassured.response.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 */
public class LevWS {
    private static final Logger logger = LoggerFactory.getLogger(LevWS.class);

    public static Response sendLEVSearchRequest(String message) {
        return RestfulUtil.sendRequest(AutomationProperties.getProperty(LEVPropertyEnum.LEV_QA1_AUTHKEY).trim(),
                AutomationProperties.getProperty(LEVPropertyEnum.LEV_QA1_SEARCH_URI).trim(),
                message);
    }

    public static Response sendLEVSearchRequest(String message, String authenticationKey, String uri) {
        return RestfulUtil.sendRequest(authenticationKey, uri, message);
    }

    public static Response sendProfileRequest(String message){
        return RestfulUtil.sendRequest(AutomationProperties.getProperty(LEVPropertyEnum.LEV_QA1_AUTHKEY).trim(),
                AutomationProperties.getProperty(LEVPropertyEnum.LEV_QA1_PROFILE_REQUEST_URI).trim(),
                message);
    }

    public static Response sendProfileRequest(String message, String authenticationKey, String uri){
        return RestfulUtil.sendRequest(authenticationKey, uri, message);
    }

    public static Response sendGetDataRequest(String message) {
        return RestfulUtil.sendRequest(AutomationProperties.getProperty(LEVPropertyEnum.LEV_QA1_AUTHKEY).trim(),
                AutomationProperties.getProperty(LEVPropertyEnum.LEV_QA1_GET_DATA_URI).trim(),
                message);
    }

    public static Response sendGetDataRequest(String message, String authenticationKey, String uri){
        return RestfulUtil.sendRequest(authenticationKey, uri, message);
    }

    public static Response sendGetPdfRequest(String message) {
        return RestfulUtil.sendRequest(AutomationProperties.getProperty(LEVPropertyEnum.LEV_QA1_AUTHKEY).trim(),
                AutomationProperties.getProperty(LEVPropertyEnum.LEV_QA1_GET_PDF_URI).trim(),
                message);
    }

    public static Response sendGetPdfRequest(String message, String authenticationKey, String uri){
        return RestfulUtil.sendRequest(authenticationKey, uri, message);
    }
}
