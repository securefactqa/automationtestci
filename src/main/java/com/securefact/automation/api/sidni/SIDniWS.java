package com.securefact.automation.api.sidni;

import static io.restassured.RestAssured.given;

import com.securefact.automation.util.RestfulUtil;
import com.securefact.automation.util.property.AutomationProperties;
import com.securefact.automation.util.property.SIDniPropertyEnum;
import io.restassured.response.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Author: William Guo
 */
public class SIDniWS {
    private static final Logger logger = LoggerFactory.getLogger(SIDniWS.class);

    public static Response sendSIDniRequest(String message) {
        return RestfulUtil.sendRequest(AutomationProperties.getProperty(SIDniPropertyEnum.SIDNI_QA1_WS_AUTHKEY).trim(),
                AutomationProperties.getProperty(SIDniPropertyEnum.SIDNI_QA1_WS_URI).trim(),
                message);
    }

    public static Response sendSIDniRequest(String message, String key, String sidniUri) {
        return RestfulUtil.sendRequest(key, sidniUri, message);
    }
}
