package com.securefact.automation.pojo.lev;

import com.securefact.automation.util.JsonUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class LevRequest {
    private String country;
    private String entityNumber;
    private String jurisdiction;
    private String entityName;
    private String customerReference;
    private String searchType;
    private String formationDate;
    private String entityType;
    private String address;
    private List<HashMap<String,String>> applicant;


    public void createEmptyList() {
        this.applicant = new ArrayList<HashMap<String, String>>() ;
    }

    public void addApplication(HashMap<String,String> applicantObject) {
        this.applicant.add(applicantObject);
    }

    public String getSearchType() {
        return searchType;
    }

    public void setSearchType(String searchType) {
        this.searchType = searchType;
    }

    public String getFormationDate() {
        return formationDate;
    }

    public void setFormationDate(String formationDate) {
        this.formationDate = formationDate;
    }

    public String getEntityType() {
        return entityType;
    }

    public void setEntityType(String entityType) {
        this.entityType = entityType;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }



    public String getCustomerReference() {
        return customerReference;
    }

    public void setCustomerReference(String customerReference) {
        this.customerReference = customerReference;
    }

    public String getCountry() {
        return country;
    }

    public LevRequest setCountry(String country) {
        this.country = country;
        return this;
    }

    public String getEntityNumber() {
        return entityNumber;
    }

    public LevRequest setEntityNumber(String entityNumber) {
        this.entityNumber = entityNumber;
        return this;
    }

    public String getJurisdiction() {
        return jurisdiction;
    }

    public LevRequest setJurisdiction(String jurisdiction) {
        this.jurisdiction = jurisdiction;
        return this;
    }

    public String getEntityName() {
        return entityName;
    }

    public LevRequest setEntityName(String entityName) {
        this.entityName = entityName;
        return this;
    }

    public String toJson() {
        return JsonUtil.convertObjToJson(this);
    }
}
